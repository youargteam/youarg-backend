"""youargBackend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
from rest_framework.documentation import include_docs_urls


urlpatterns = [
    url(r'^admin/', admin.site.urls, name='admin'),
    url(r'^api/', include_docs_urls(title='Youarg backend'), name='api'),
    url(r'^user/', include(('youargBackend.apps.user.urls', 'user'))),
    url(r'^node/', include(('youargBackend.apps.node.urls', 'node'))),
]


import allauth.urls

urlpatterns += allauth.urls.urlpatterns
