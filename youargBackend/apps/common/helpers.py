from rest_framework.serializers import CurrentUserDefault


class CurrentUserProfileDefault(CurrentUserDefault):
    def __call__(self):
        return self.user.userprofile
