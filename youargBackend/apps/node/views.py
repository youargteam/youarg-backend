from rest_framework import generics
from rest_framework.response import Response
from rest_framework import mixins
from rest_framework.exceptions import APIException
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from .serializers import *
from .permissions import *
from ..common import pagination
from ..user import karma_type

NODE_CHILDREN_AMOUNT_IN_TREE = 3

# Node


class NodeRUDView(generics.RetrieveUpdateDestroyAPIView):
    """
    Просмотр, редактирование и удаление ноды
    """
    queryset = Node.objects.all().order_by('created').reverse()
    serializer_class = NodeUSerializer
    permission_classes = [IsOwnerOrReadOnly, KarmaCreateNodeOrReadOnly]


class NodeCView(generics.CreateAPIView):
    """
    Создание ноды
    """
    serializer_class = NodeSerializer
    permission_classes = [IsAuthenticatedOrReadOnly,
                          KarmaCreateNodeOrReadOnly,
                          NodeAmountLimitOrReadOnly]


class NodeParentListView(generics.ListAPIView):
    """
    Получить список тем(нод без родителей)
    """
    queryset = Node.objects.filter(parent=None).order_by('created').reverse()
    serializer_class = NodeSerializer
    pagination = pagination.StandardResultsSetPagination


class NodeTreeListView(generics.ListAPIView):
    """
    Получить неполное дерево детей ноды
    """
    serializer_class = NodeTreeSerializer
    pagination = pagination.StandardResultsSetPagination

    def get_queryset(self):
        pk = self.kwargs['pk']
        nodes = Node.get_interesting().filter(parent=pk)

        return nodes


# Importance


class NodeImportanceC(generics.CreateAPIView):
    """
    Поставить отметку важности
    """
    serializer_class = NodeImportanceSerializer
    permission_classes = [IsAuthenticatedOrReadOnly,
                          KarmaImportancePutOrReadOnly,
                          NodeImportanceAmountLimitOrReadOnly]


class NodeImportanceRUD(generics.RetrieveUpdateDestroyAPIView):
    """
    Обновить/удалить пометку важности
    """
    serializer_class = NodeImportanceUSerializer
    queryset = NodeImportance.objects.all()
    permission_classes = [IsOwnerOrReadOnly, KarmaImportancePutOrReadOnly]


# Report


class ReportL(generics.ListAPIView):
    """
    Получить список типов репорта
    """
    queryset = ""
    serializer_class = ReportSerializer
    pagination_class = None

    def list(self, request, *args, **kwargs):
        serializer = self.get_serializer(karma_type.KARMA_TYPES, many=True)
        return Response(serializer.data)
