# Generated by Django 2.0.1 on 2018-03-03 17:15

from django.db import migrations, models
import django.db.models.deletion
import mptt.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('node', '0001_initial'),
        ('user', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='nodereport',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user.UserProfile'),
        ),
        migrations.AddField(
            model_name='nodeimportance',
            name='node',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='node.Node'),
        ),
        migrations.AddField(
            model_name='nodeimportance',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user.UserProfile'),
        ),
        migrations.AddField(
            model_name='node',
            name='nested_node',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='nested', to='node.Node'),
        ),
        migrations.AddField(
            model_name='node',
            name='parent',
            field=mptt.fields.TreeForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='children', to='node.Node'),
        ),
        migrations.AddField(
            model_name='node',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user.UserProfile'),
        ),
        migrations.AlterUniqueTogether(
            name='nodeimportance',
            unique_together={('user', 'node')},
        ),
    ]
