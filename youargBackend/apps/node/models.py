from django.db import models
from model_utils import FieldTracker
from django.contrib.auth.models import User
from mptt.models import MPTTModel, TreeForeignKey
from ..user import models as user_models
from ..user import karma_type


class Node(MPTTModel):
    user = models.ForeignKey(user_models.UserProfile, null=False, on_delete=models.CASCADE)
    text = models.TextField(max_length=1024, blank=False)
    argument = models.BooleanField(blank=False)
    nested_node = models.ForeignKey(
        'self',
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='nested'
    )
    parent = TreeForeignKey(
        'self',
        null=True,
        blank=True,
        related_name='children',
        db_index=True,
        on_delete=models.deletion.CASCADE
    )
    created = models.DateTimeField(auto_now_add=True, blank=False)

    importance_sum = models.IntegerField(default=0)
    importance_amount = models.IntegerField(default=0)
    truthfulness_sum = models.FloatField(default=0)
    truthfulness_amount = models.FloatField(default=0)

    tracker = FieldTracker()

    class MPTTMeta:
        order_insertion_by = ['created']

    @staticmethod
    def get_interesting():
        """
        Возвращает массив с нодами с сортировкой по интересности
        TODO: Перенести в Manager
        """
        return Node.objects.extra(
            select={
                'interest':
                    '(importance_sum / greatest(importance_amount, 1)) * '
                    '(((truthfulness_sum / greatest(truthfulness_amount, 1)) + 1) / 2 * 100)'}
        ).extra(order_by=['-interest'])

    def __str__(self):
        return self.text


class NodeChildren:
    NODE_CHILDREN_AMOUNT_IN_TREE = 3

    count = None
    results = None

    def __init__(self, parent):
        """
        Получение детей ноды с ограничением NODE_CHILDREN_AMOUNT_IN_TREE
        внутри объекта с (параметром) настоящим количеством детей ноды
        TODO: Реализовать алгоритм для наиболее важных нод
        """
        nodes = Node.get_interesting().filter(parent=parent)
        self.count = nodes.count()
        self.results = nodes[:self.NODE_CHILDREN_AMOUNT_IN_TREE]


class NodeImportance(models.Model):
    user = models.ForeignKey(user_models.UserProfile, null=False, on_delete=models.CASCADE)
    node = models.ForeignKey(Node, null=False, on_delete=models.CASCADE)
    value = models.IntegerField()
    created = models.DateTimeField(auto_now_add=True, blank=False)

    tracker = FieldTracker()

    class Meta:
        unique_together = (("user", "node"),)

    def __str__(self):
        return self.user.__str__()


class NodeReport(models.Model):
    user = models.ForeignKey(user_models.UserProfile, null=False, on_delete=models.CASCADE)
    node = models.ForeignKey(
        Node,
        null=False,
        on_delete=models.CASCADE,
        related_name='node'
    )
    node_second = models.ForeignKey(
        Node,
        null=True,
        on_delete=models.CASCADE,
        related_name='node_second'
    )
    report_type_name = models.CharField(max_length=256)
    description = models.CharField(max_length=1024)

    def __str__(self):
        return "{0} - {1}".format(self.report_type_name, self.node.__str__())

from . import model_signals  # handle insert, update, delete logic