from rest_framework.test import APITestCase
from django.urls import reverse
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
import json
from .models import *
from ..user.signals import *
from ..user.karma_type import *
from . import permissions


class NodeAPIViewTestCase(APITestCase):
    url_c = 'node:node-create'
    url_rud = 'node:node-rud'
    url_parents = 'node:node-parents'
    url_tree = 'node:node-tree'
    url_importance_rud = 'node:node-importance-rud'
    url_importance_create = 'node:node-importance-create'

    # owns self.node and self.importance
    user_owner = None
    token_owner = None

    user_high_karma = None
    token_high_karma = None

    user_low_karma = None
    token_low_karma = None

    def setUp(self):
        (self.user_owner, self.token_owner) = self.create_user(name='Owner', karma=100)
        (self.user_high_karma, self.token_high_karma) = self.create_user(name='Karma high', karma=100)
        (self.user_low_karma, self.token_low_karma) = self.create_user(name='Karma low', karma=-100)

        self.node = Node.objects.create(user=self.user_owner.userprofile, text='hi mom', argument=False)
        self.node1 = Node.objects.create(user=self.user_owner.userprofile, text='hi mom', argument=False)
        self.importance = NodeImportance.objects.create(user=self.user_owner.userprofile, node=self.node1, value=50)

    # node

    def test_create_node(self):
        self.create_permissions(self.create_node)

    def test_create_node_max_amount(self):
        self.max_amount(permissions.NodeAmountLimitOrReadOnly.max_amount, self.create_node)

    def test_node_karma(self):
        self.karma_change(
            KarmaTypeNames.REWARD_CREATE_NODE,
            self.create_node,
            self.update_node,
            self.delete_node
        )

    def test_read_node(self):
        url = reverse(self.url_rud, args=[self.node.pk])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_update_node(self):
        self.update_permissions(obj=self.node, update_method=self.update_node)

    def test_delete_node(self):
        self.delete_permissions(obj=self.node, delete_method=self.delete_node)

    def test_parents(self):
        url = reverse(self.url_parents)
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_tree(self):
        url = reverse(self.url_tree, args=[self.node.pk])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    # node importance

    def test_create_importance(self):
        self.create_permissions(self.create_importance_with_node)

    def test_create_importance_max_amount(self):
        self.max_amount(permissions.NodeImportanceAmountLimitOrReadOnly.max_amount, self.create_importance_unique)

    def test_importance_karma(self):
        self.karma_change(
            KarmaTypeNames.REWARD_IMPORTANCE_CHECK,
            self.create_importance_with_node,
            self.update_importance,
            self.delete_importance
        )

    def test_read_importance(self):
        url = reverse(self.url_importance_rud, args=[self.importance.pk])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_update_importance(self):
        self.update_permissions(obj=self.importance, update_method=self.update_importance)

    def test_delete_importance(self):
        self.delete_permissions(obj=self.importance, delete_method=self.delete_importance)

    # truthfulness

    def test_truthfulness(self):
        self.check_truthfulness(100, self.node)

        # counter argument
        self.add_node(False, self.node, 'counter argument')
        self.check_truthfulness(0, self.node)

        # argument
        node_argument = self.add_node(True, self.node, 'argument')
        self.check_truthfulness(50, self.node)

        # edit node
        node_argument.argument = False
        node_argument.save()
        self.check_truthfulness(0, self.node)

        node_argument.argument = True
        node_argument.save()
        self.check_truthfulness(50, self.node)

        # nested
        self.add_node(False, node_argument, 'nested counter argument')
        self.check_truthfulness(0, self.node)
        node_nested_argument = self.add_node(True, node_argument, 'nested argument')
        self.check_truthfulness(16, self.node)

        # importance
        importance1 = self.add_importance(node_nested_argument, 0, self.user_owner)
        self.check_truthfulness(0, self.node)
        importance2 = self.add_importance(node_nested_argument, 100, self.user_high_karma)
        self.check_truthfulness(8, self.node)

        low_importance_node = self.add_node(True, None, "Low importance test")
        low_importance_argument = self.add_node(True, low_importance_node, 'Low Importance argument')
        self.add_importance(low_importance_argument, 50, self.user_owner)
        self.check_truthfulness(100, low_importance_node)
        self.add_node(False, low_importance_node, "Low importance counter argument")
        self.check_truthfulness(33, low_importance_node)

        # edit importance
        importance2.value = 0
        importance2.save()
        self.check_truthfulness(0, self.node)

        # delete importance
        importance1.delete()
        importance2.delete()
        self.check_truthfulness(16, self.node)

        # edit node
        node_argument.text = 'argument edited'
        node_argument.save()
        self.check_truthfulness(16, self.node)

        # delete node
        node_argument.delete()
        self.check_truthfulness(0, self.node)


    # helper methods

    def create_permissions(self, create_method):
        # unauthorized
        create_method(401)

        # low karma
        self.api_authentication(token=self.token_low_karma)
        create_method(403)

        # high karma
        self.api_authentication(token=self.token_high_karma)
        create_method(201)

    def update_permissions(self, obj, update_method):
        # unauthorized
        update_method(obj=obj, expected_status=401)

        # owner
        self.api_authentication(self.token_owner)
        update_method(obj=obj, expected_status=200)

        # not owner
        self.api_authentication(self.token_high_karma)
        update_method(obj=obj, expected_status=403)

    def delete_permissions(self, obj, delete_method):
        # unauthorized
        delete_method(obj=obj, expected_status=401)

        # not owner
        self.api_authentication(self.token_high_karma)
        delete_method(obj=obj, expected_status=403)

        # owner
        self.api_authentication(self.token_owner)
        delete_method(obj=obj, expected_status=204)

    def create_user(self, name, karma):
        user = User(username=name)
        user.save()

        user.userprofile.karma_cached = karma
        user.userprofile.save()

        token = Token.objects.get(user=user)
        return user, token

    def api_authentication(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

    def get_node_truthfulness(self, node):
        url = reverse(self.url_rud, args=[node.pk])
        response = self.client.get(url)
        return response.data['truthfulness']

    def check_truthfulness(self, expected_truthfulness, check_node):
        truthfulness = self.get_node_truthfulness(check_node)
        self.assertEqual(expected_truthfulness, truthfulness)

    def add_node(self, argument, parent_node, name='hi'):
        return Node.objects.create(
            user=self.user_owner.userprofile,
            text=name,
            argument=argument,
            parent=parent_node
        )

    def create_node(self, expected_status=201):
        url = reverse(self.url_c)
        response = self.client.post(url, {
            'text': 'clean the room',
            'parent': self.node1.pk,
            'argument': True
        })
        self.assertEqual(expected_status, response.status_code)

    def update_node(self, obj=None, expected_status=200):
        obj = obj or self.node

        url = reverse(self.url_rud, args=[obj.pk])
        response = self.client.patch(url, {'argument': True, 'text': 'bla'})
        self.assertEqual(expected_status, response.status_code)
        return response

    def delete_node(self, obj=None, expected_status=204):
        obj = obj or self.node

        url = reverse(self.url_rud, args=[obj.pk])
        response = self.client.delete(url)
        self.assertEqual(expected_status, response.status_code)

    def add_importance(self, node, value, user):
        return NodeImportance.objects.create(node=node, value=value, user=user.userprofile)

    def create_importance_with_node(self, expected_status=201, node=None):
        node = node or self.node

        url = reverse(self.url_importance_create)
        response = self.client.post(url, {
            'value': 50,
            'node_pk': node.pk
        })
        self.assertEqual(expected_status, response.status_code)

    def create_importance_unique(self, expected_status=201):
        node = Node(text='clean the room', argument=True, user=self.user_owner.userprofile)
        node.save()
        self.create_importance_with_node(node=node, expected_status=expected_status)

    def update_importance(self, obj=None, expected_status=200):
        obj = obj or self.importance
        url = reverse(self.url_importance_rud, args=[obj.pk])
        response = self.client.patch(url, {'value': 100})
        self.assertEqual(expected_status, response.status_code)
        return response

    def delete_importance(self, obj=None, expected_status=204):
        obj = obj or self.importance
        url = reverse(self.url_importance_rud, args=[obj.pk])
        response = self.client.delete(url)
        self.assertEqual(expected_status, response.status_code)

    def max_amount(self, amount, create_method):
        self.api_authentication(self.token_high_karma)
        for _ in range(amount):
            create_method(expected_status=201)
        create_method(expected_status=403)

    def karma_change(self, karma_type_name, create_method, update_method, delete_method):
        self.api_authentication(self.token_owner)

        karma_amount = KarmaType.get_karma_type(karma_type_name).amount
        initial_karma = self.user_owner.userprofile.karma_cached
        added_karma = initial_karma + karma_amount

        self.check_karma(update_method, initial_karma)
        self.check_karma(create_method, added_karma)
        self.check_karma(delete_method, initial_karma)

    def check_karma(self, method, expected_karma):
        method()
        self.user_owner.refresh_from_db()
        real_karma = self.user_owner.userprofile.karma_cached
        self.assertEqual(expected_karma, real_karma)
