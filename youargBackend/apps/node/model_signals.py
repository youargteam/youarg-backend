from django.db import models
from django.db.models.signals import *
from django.dispatch import receiver
from . import models
from ..user import models as user_models
from ..user import karma_type
from copy import copy


# node


@receiver(post_save, sender=models.Node)
def post_save_node(instance, created, **kwargs):
    if created:
        user_models.UserKarma.objects.create(
            user=instance.user,
            karma_type=karma_type.KarmaTypeNames.REWARD_CREATE_NODE.value,
            node=instance
        )
        add_node_update_truthfulness(instance)
    else:
        updated_node_update_truthfulness(instance)


@receiver(post_delete, sender=models.Node)
def delete_node(instance, **kwargs):
    deleted_node_update_truthfulness(instance)


# importance


@receiver(post_save, sender=models.NodeImportance)
def on_save_importance(instance, created, **kwargs):
    node_old_truthfulness, node_old_truthfulness_amount = get_truthfulness(instance.node)

    if created:
        add_importance(instance)
    else:
        update_importance(instance)

    update_truthfulness(instance.node, node_old_truthfulness, node_old_truthfulness_amount)


@receiver(pre_delete, sender=models.NodeImportance)
def on_delete_importance(instance, **kwargs):
    node_old_truthfulness, node_old_truthfulness_amount = get_truthfulness(instance.node)

    delete_importance(instance)

    update_truthfulness(instance.node, node_old_truthfulness, node_old_truthfulness_amount)


# helper methods


def add_importance(importance):
    user_models.UserKarma.objects.create(
        user=importance.user,
        karma_type=karma_type.KarmaTypeNames.REWARD_IMPORTANCE_CHECK.value,
        importance=importance
    )

    importance.node.importance_sum += importance.value
    importance.node.importance_amount += 1
    importance.node.save()


def update_importance(importance):
    importance.node.importance_sum -= importance.tracker.previous('value')
    importance.node.importance_sum += importance.value
    importance.node.save()


def delete_importance(importance):
    importance.node.importance_amount -= 1
    importance.node.importance_sum -= importance.value
    importance.node.save()


# truthfulness


def add_node_update_truthfulness(node):
    parent = node.parent
    if parent is None:
        return

    parent_old_truthfulness, parent_old_truthfulness_amount = get_truthfulness(parent)

    truthfulness, truthfulness_amount = get_truthfulness(node)
    parent.truthfulness_sum += truthfulness
    parent.truthfulness_amount += truthfulness_amount
    parent.save()

    update_truthfulness(parent, parent_old_truthfulness, parent_old_truthfulness_amount)


def updated_node_update_truthfulness(node):
    # imitate old state of a node to receive truthfulness
    old_node = copy(node)
    old_node.argument = node.tracker.previous('argument')
    # if nothing important for truthfulness is changed we can just skip
    if old_node.argument == node.argument:
        return
    truthfulness, truthfulness_amount = get_truthfulness(old_node)

    update_truthfulness(node, truthfulness, truthfulness_amount)


def deleted_node_update_truthfulness(node):
    parent_pk = node.parent_id
    parent = models.Node.objects.filter(pk=parent_pk).last()
    if parent is None:
        return

    parent_old_truthfulness, parent_old_truthfulness_amount = get_truthfulness(parent)

    truthfulness, truthfulness_amount = get_truthfulness(node)
    parent.truthfulness_sum -= truthfulness
    parent.truthfulness_amount -= truthfulness_amount
    parent.save()

    update_truthfulness(parent, parent_old_truthfulness, parent_old_truthfulness_amount)


def update_truthfulness(node, old_truthfulness_sum, old_truthfulness_amount):
    parent = node.parent
    if parent is None:
        return

    parent_old_truthfulness, parent_old_truthfulness_amount = get_truthfulness(parent)

    truthfulness, truthfulness_amount = get_truthfulness(node)
    parent.truthfulness_sum -= old_truthfulness_sum
    parent.truthfulness_sum += truthfulness

    parent.truthfulness_amount -= old_truthfulness_amount
    parent.truthfulness_amount += truthfulness_amount

    parent.save()

    update_truthfulness(parent, parent_old_truthfulness, parent_old_truthfulness_amount)


def get_truthfulness(node):
    # if no truthfulness information on node consider it true
    truthfulness_sum = 1
    truthfulness_amount = 1
    if node.truthfulness_amount != 0:
        truthfulness_sum = node.truthfulness_sum
        truthfulness_amount = node.truthfulness_amount

    from .serializers import MAXIMUM_IMPORTANCE_VALUE

    importance = 1
    if node.importance_amount != 0:
        importance = (node.importance_sum / node.importance_amount) / MAXIMUM_IMPORTANCE_VALUE

    truthfulness = truthfulness_sum / truthfulness_amount
    truthfulness_amount = get_truthfulness_amount(truthfulness) * importance
    truthfulness *= 1 if node.argument else -1
    truthfulness *= truthfulness_amount
    return truthfulness, truthfulness_amount


def get_truthfulness_amount(truthfulness):
    return (truthfulness + 1) / 2
