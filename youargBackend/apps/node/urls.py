from django.conf.urls import url, include
from rest_framework import routers
from . import views


# router = routers.DefaultRouter()
# router.register(r'', views.NodeRUD)

urlpatterns = [
    url(r'^(?P<pk>\d+)/$',
        views.NodeRUDView.as_view(),
        name='node-rud'),

    url(r'^$',
        views.NodeCView.as_view(),
        name='node-create'),

    url(r'^parents/$',
        views.NodeParentListView.as_view(),
        name='node-parents'),

    url(r'^tree/(?P<pk>\d+)/$',
        views.NodeTreeListView.as_view(),
        name='node-tree'),

    url(r'^importance/(?P<pk>\d+)/$',
        views.NodeImportanceRUD.as_view(),
        name='node-importance-rud'),

    url(r'^importance/$',
        views.NodeImportanceC.as_view(),
        name='node-importance-create'),
]
