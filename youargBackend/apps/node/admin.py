from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.Node)
admin.site.register(models.NodeImportance)
admin.site.register(models.NodeReport)