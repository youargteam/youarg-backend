from enum import Enum


class ReportType:
    def __init__(self, name, short_desc, karma_punishment):
        self.name = name
        self.short_desc = short_desc
        self.karma_punishment = karma_punishment

    @staticmethod
    def get_report_type(by_name):
        name_string = by_name if type(by_name) == type("") else by_name.value
        for report_type in REPORT_TYPES:
            if report_type.name == name_string:
                return report_type


class ReportTypeName(Enum):
    MULTIPLE_ARGUMENTS = 'MULTIPLE_ARGUMENTS'
    NO_INFO = 'NO_INFO'
    DISTORTION = 'DISTORTION'
    REPEAT = 'REPEAT'
    INCORRECT_TYPE = 'INCORRECT_TYPE'


REPORT_TYPES = [
    ReportType(
        ReportTypeName.MULTIPLE_ARGUMENTS.value,
        "В одной ноде больше одного аргумента",
        200
    ),

    ReportType(
        ReportTypeName.NO_INFO.value,
        "Нода не содержит аргумента",
        250
    ),

    ReportType(
        ReportTypeName.DISTORTION.value,
        "Искажение слов или не имение отношения к отвечаемой ноде",
        300
    ),

    ReportType(
        ReportTypeName.REPEAT.value,
        "Повторение существующей ноды",
        40
    ),

    ReportType(
        ReportTypeName.INCORRECT_TYPE.value,
        "Неправильно выбран тип ноды. 'За' вместо 'Против', или наоборот.",
        50
    )
]
