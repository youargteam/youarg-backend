from rest_framework import permissions
from ..user.karma_levels import *
from django.utils import timezone
from .models import *


class IsOwnerOrReadOnly(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Write permissions are only allowed to the owner.
        return obj.user == request.user.userprofile


# karma


class _KarmaLevelOrReadOnly(permissions.BasePermission):
    karma_level = None

    def has_permission(self, request, view):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        if not isinstance(request.user, User):
            return False

        return KarmaLevel.is_user_allowed(request.user.userprofile, self.karma_level)


class KarmaCreateNodeOrReadOnly(_KarmaLevelOrReadOnly):
    karma_level = KarmaLevel.get_karma_level(KarmaLevelName.REPLY)
    message = "Недостаточно кармы для создания ноды"


class KarmaImportancePutOrReadOnly(_KarmaLevelOrReadOnly):
    karma_level = KarmaLevel.get_karma_level(KarmaLevelName.IMPORTANCE)
    message = "Недостаточно кармы для выставления важности"


class KarmaEffectReportOrReadOnly(_KarmaLevelOrReadOnly):
    karma_level = KarmaLevel.get_karma_level(KarmaLevelName.EFFECT_REPORT)
    message = "Недостаточно кармы для голосования по репорту"


class KarmaReportOrReadOnly(_KarmaLevelOrReadOnly):
    karma_level = KarmaLevel.get_karma_level(KarmaLevelName.REPORT)
    message = "Недостаточно кармы для репорта"


# objects amount per user permissions


class _AmountPerUserLimitOrReadOnly(permissions.BasePermission):
    """
    user objects 'max_amount' limit per some 'delta_hours'
    'model_class' has to have created field
    'model_class' has to have user field
    """
    max_amount = 0
    model_class = None
    delta_hours = 0
    delta_minutes = 0
    message = ""
    message_format = "Вы исчерпали лимит. Осталось {:02}:{:02}:{:02}"

    def has_permission(self, request, view):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        if not isinstance(request.user, User):
            return False

        return self.check_for_amount_limit(request)

    def check_for_amount_limit(self, request):
        objects_per_time = self.model_class.objects.filter(
            created__gte=self.get_time_from(),
            created__lt=timezone.now(),
            user=request.user.pk
        ).order_by('created')

        amount_per_time = objects_per_time.count()

        if amount_per_time >= self.max_amount:
            self.set_message(objects_per_time.first().created)
            return False

        return True

    def get_time_from(self):
        return timezone.now() + timezone.timedelta(
            hours=-self.delta_hours,
            minutes=-self.delta_minutes
        )

    def set_message(self, time):
        time_left = time - self.get_time_from()
        self.message = self.message_format.format(
            time_left.seconds // 3600, (time_left.seconds // 60) % 60, time_left.seconds % 60
        )


class NodeAmountLimitOrReadOnly(_AmountPerUserLimitOrReadOnly):
    max_amount = 5
    model_class = Node
    # delta_minutes = 5
    delta_minutes = 1


class NodeImportanceAmountLimitOrReadOnly(_AmountPerUserLimitOrReadOnly):
    max_amount = 5
    model_class = NodeImportance
    # delta_hours = 1
    delta_minutes = 1


class ReportAmountLimitOrReadOnly(_AmountPerUserLimitOrReadOnly):
    max_amount = 5
    model_class = NodeReport
    # delta_hours = 1
    delta_minutes = 1
