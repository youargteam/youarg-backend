from rest_framework import serializers
from django.core import serializers as core_serializers
from . import models
from ..common.helpers import CurrentUserProfileDefault
from ..user import serializers as user_serializers
from ..user import models as user_models
from ..user import karma_type


MINIMUM_IMPORTANCE_VALUE = 0
MAXIMUM_IMPORTANCE_VALUE = 100


# node


class NodeSerializer(serializers.ModelSerializer):
    user_pk = serializers.HiddenField(
        default=CurrentUserProfileDefault(),
        source='user'
    )
    user = user_serializers.UserSerializer(read_only=True)
    truthfulness = serializers.SerializerMethodField()
    my_importance = serializers.SerializerMethodField()

    class Meta:
        model = models.Node
        fields = ('pk', 'text',
                  'parent', 'argument',
                  'user', 'user_pk',
                  'created',
                  'importance_sum', 'importance_amount',
                  'truthfulness', 'my_importance')
        read_only_fields = ('created', 'importance_sum', 'importance_amount')

    def get_truthfulness(self, obj):
        if obj.truthfulness_amount == 0:
            return 100
        truthfulness = obj.truthfulness_sum / obj.truthfulness_amount
        return int((truthfulness + 1) / 2 * 100)

    def get_my_importance(self, obj):
        importance_json = None
        try:
            user = self.context['request'].user
            importance = models.NodeImportance.objects.get(user=user.pk, node=obj)
            importance_json = NodeImportanceSimpleSerializer(instance=importance).data
        finally:
            return importance_json


class NodeUSerializer(NodeSerializer):
    class Meta(NodeSerializer.Meta):
        read_only_fields = NodeSerializer.Meta.read_only_fields + ('parent',)


class NodeChildrenSerializer(serializers.Serializer):
    count = serializers.IntegerField()
    parent = NodeSerializer(many=False, read_only=True)
    results = NodeSerializer(many=True, read_only=True)

    class Meta:
        fields = ('count', 'results')

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class NodeTreeSerializer(NodeSerializer):
    children = serializers.SerializerMethodField()

    class Meta(NodeSerializer.Meta):
        fields = NodeSerializer.Meta.fields + ('children',)

    def get_children(self, obj):
        instance = models.NodeChildren(parent=obj)
        return NodeChildrenSerializer(instance=instance, context=self.context).data


# node importance


class NodeImportanceSimpleSerializer(serializers.ModelSerializer):
    user_pk = serializers.HiddenField(
        default=CurrentUserProfileDefault(),
        source='user'
    )
    node_pk = serializers.PrimaryKeyRelatedField(
        write_only=True,
        source='node',
        queryset=models.Node.objects.all()
    )

    class Meta:
        model = models.NodeImportance
        fields = ('pk',
                  'user_pk',
                  'node_pk',
                  'value')

    def validate_value(self, value):
        if value < MINIMUM_IMPORTANCE_VALUE or value > MAXIMUM_IMPORTANCE_VALUE:
            raise serializers.ValidationError(
                "{0} не входит в допустимый промежуток важности ({1}-{2})".format(
                    value, MINIMUM_IMPORTANCE_VALUE, MAXIMUM_IMPORTANCE_VALUE
                )
            )
        return value


class NodeImportanceSerializer(NodeImportanceSimpleSerializer):
    node = NodeSerializer(read_only=True)
    user = user_serializers.UserSerializer(read_only=True)

    class Meta(NodeImportanceSimpleSerializer.Meta):
        fields = NodeImportanceSimpleSerializer.Meta.fields + ('node', 'user')


class NodeImportanceUSerializer(NodeImportanceSerializer):
    class Meta(NodeImportanceSerializer.Meta):
        read_only_fields = ('user', 'node',)


# node report


class ReportSerializer(serializers.SerializerMetaclass):
    name = serializers.CharField()
    short_desc = serializers.CharField()
    karma_punishment = serializers.IntegerField()


class NodeReportSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.NodeReport
        fields = (
            'pk',
            'node',
            'node_second',
            'report_type_name',
            'description'
        )
