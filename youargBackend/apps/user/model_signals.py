from django.db import models
from django.db.models.signals import *
from django.dispatch import receiver
from .models import *
from .karma_type import *


@receiver(post_save, sender=UserKarma)
def save(instance, created, **kwargs):
    if created:
        update_cached_karma(instance.user, instance.karma_type)


@receiver(pre_delete, sender=UserKarma)
def delete(instance, **kwargs):
    update_cached_karma(instance.user, instance.karma_type, is_delete=True)


def update_cached_karma(user, karma_type_name, is_delete=False):
    karma_type = KarmaType.get_karma_type(karma_type_name)
    amount = karma_type.amount if karma_type.is_reward else -karma_type.amount
    amount = -amount if is_delete else amount
    user.karma_cached += amount
    user.save()
