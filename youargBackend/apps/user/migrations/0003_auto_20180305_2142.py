# Generated by Django 2.0.2 on 2018-03-05 15:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0002_userkarma_importance'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userkarma',
            name='importance',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='node.NodeImportance'),
        ),
        migrations.AlterField(
            model_name='userkarma',
            name='node',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='node.Node'),
        ),
    ]
