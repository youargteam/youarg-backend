# Generated by Django 2.0.1 on 2018-02-16 17:49

from django.db import migrations, models
import django.utils.timezone
from youargBackend.youargBackend.apps.user.models import *
from youargBackend.youargBackend.apps.user.karma_type import *


def update_users_carma(apps, schema_editor):
    for user_profile in UserProfile.objects.all():
        user_profile.karma_cached = 0
        for user_karma in UserKarma.objects.filter(user=user_profile.user):
            karma_type_name = KarmaTypeNames(user_karma.karma_type)
            UserKarma.update_cached_karma(user_profile.user, karma_type_name)


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0007_userkarma_created')
    ]

    operations = [
        migrations.RunPython(update_users_carma)
    ]
