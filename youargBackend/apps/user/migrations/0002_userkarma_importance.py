# Generated by Django 2.0.2 on 2018-03-05 10:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('node', '0002_auto_20180303_2315'),
        ('user', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='userkarma',
            name='importance',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='node.NodeImportance'),
        ),
    ]
