from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from django.urls import reverse


class UserTestCase(APITestCase):
    url_token = 'user:token'
    url_self = 'user:user-get'
    url_karma_type = 'user:karma-type-list'
    url_karma_type_self = 'user:karma-type-self-list'
    url_karma_level = 'user:karma-level-list'
    url_karma_level_self = 'user:karma-level-self-list'

    user = None
    token = None

    def setUp(self):
        (self.user, self.token) = self.create_user('hf', 0)

    """
    'user:token' does not get user for some reason, only in tests
    def test_token(self):
        self.api_authentication(self.token)
        url = reverse(self.url_token)
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
    """

    def test_self_get(self):
        self.simple_get_method(self.url_self)

    def test_karma_type(self):
        self.simple_get_method(self.url_karma_type)

    def test_karma_type_self(self):
        self.simple_get_method(self.url_karma_type_self)

    def test_karma_level(self):
        self.simple_get_method(self.url_karma_level)

    def test_karma_level_self(self):
        self.simple_get_method(self.url_karma_level_self)

    # helper methods

    def api_authentication(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

    def create_user(self, name, karma):
        user = User(username=name)
        user.save()

        user.userprofile.karma_cached = karma
        user.userprofile.save()

        token = Token.objects.get(user=user)
        return user, token

    def simple_get_method(self, url_str, expected_status=200):
        self.api_authentication(self.token)
        url = reverse(url_str)
        response = self.client.get(url)
        self.assertEqual(expected_status, response.status_code)