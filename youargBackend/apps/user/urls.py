from django.conf.urls import *
from . import views
from . import signals


urlpatterns = [
    url(r'^token$',
        views.pass_token,
        name='token'),

    url(r'^self$',
        views.UserGet.as_view(),
        name='user-get'),

    url(r'^karma-type/$',
        views.KarmaTypeL.as_view(),
        name='karma-type-list'),

    url(r'^karma-type-self/$',
        views.KarmaTypeSelfR.as_view(),
        name='karma-type-self-list'),

    url(r'^karma-level/$',
        views.KarmaLevelL.as_view(),
        name='karma-level-list'),

    url(r'^karma-level-self/$',
        views.KarmaLevelsSelfL.as_view(),
        name='karma-level-self-list'),
]

