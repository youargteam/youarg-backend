from rest_framework import serializers
from django.contrib.auth.models import User
from allauth.socialaccount.models import SocialAccount
from . import models
from django.core.exceptions import ObjectDoesNotExist
from .karma_type import *


class UserSerializer(serializers.ModelSerializer):
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    uid = serializers.SerializerMethodField()
    provider = serializers.SerializerMethodField()
    social_account = None

    class Meta:
        model = models.UserProfile
        fields = (
            'uid',
            'provider',
            'pk',
            'first_name',
            'last_name',
            'karma_cached'
        )

    def get_social_account(self, obj):
        if self.social_account:
            return self.social_account
        try:
            self.social_account = SocialAccount.objects.get(user=obj.user)
        except ObjectDoesNotExist:
            return None
        return self.social_account

    @staticmethod
    def get_first_name(obj):
        return obj.user.first_name

    @staticmethod
    def get_last_name(obj):
        return obj.user.last_name

    def get_uid(self, obj):
        social_account = self.get_social_account(obj)
        if social_account:
            return social_account.uid
        return None

    def get_provider(self, obj):
        social_account = self.get_social_account(obj)
        if social_account:
            return social_account.provider
        return None


# Karma


class KarmaTypeSerializer(serializers.Serializer):
    name = serializers.CharField()
    short_desc = serializers.CharField()
    amount = serializers.IntegerField()
    is_reward = serializers.BooleanField()

    def create(self, validated_data):
        raise serializers.ErrorDetail(
            "Невозможно создать тип кармы. Свяжитесь с разработчиками."
        )
        return None

    def update(self, instance, validated_data):
        raise serializers.ErrorDetail(
            "Невозможно обновить тип кармы. Свяжитесь с разработчиками."
        )
        return None


class UserKarmaSerializer(serializers.ModelSerializer):
    karma_type = serializers.SerializerMethodField()

    class Meta:
        model = models.UserKarma
        fields = (
            'user',
            'karma_type',
            'node',
            'created'
        )

    @staticmethod
    def get_karma_type(obj):
        karma_type = KarmaType.get_karma_type(obj.karma_type)
        return KarmaTypeSerializer(karma_type).data


class KarmaLevelSerializer(serializers.Serializer):
    name = serializers.CharField()
    short_desc = serializers.CharField()
    amount = serializers.IntegerField()

    def create(self, validated_data):
        raise serializers.ErrorDetail(
            "Невозможно создать тип кармы. Свяжитесь с разработчиками."
        )
        return None

    def update(self, instance, validated_data):
        raise serializers.ErrorDetail(
            "Невозможно обновить тип кармы. Свяжитесь с разработчиками."
        )
        return None
