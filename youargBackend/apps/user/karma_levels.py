from enum import Enum
from . import models


class KarmaLevel:
    def __init__(self, name, short_desc, amount):
        self.name = name
        self.short_desc = short_desc
        self.amount = amount

    @staticmethod
    def get_karma_level(by_name):
        name_string = by_name if type(by_name) == type("") else by_name.value
        for karma_level in KARMA_LEVELS:
            if karma_level.name == name_string:
                return karma_level

    @staticmethod
    def is_user_allowed(user, karma_level):
        user_karma_levels = KarmaLevel.get_karma_levels_self(user)
        return karma_level in user_karma_levels

    @staticmethod
    def get_karma_levels_self(user):
        user_karma = user.karma_cached
        user_karma_levels = []
        for karma_level in KARMA_LEVELS:
            if user_karma >= karma_level.amount:
                user_karma_levels.append(karma_level)
        return user_karma_levels


class KarmaLevelName(Enum):
    REPLY = "REPLY"
    IMPORTANCE = "IMPORTANCE"
    EFFECT_REPORT = "EFFECT_REPORT"
    REPORT = "REPORT"


KARMA_LEVELS = [
    KarmaLevel(KarmaLevelName.REPLY.value,
               "Разблокирована возможность создавать ноды", 0),

    KarmaLevel(KarmaLevelName.IMPORTANCE.value,
               "Разблокирована возможность ставить оценки важности", 100),

    KarmaLevel(KarmaLevelName.EFFECT_REPORT.value,
               "Разблокирована возможность подтверждать или опровергать репорты", 200),

    KarmaLevel(KarmaLevelName.REPORT.value,
               "Разблокирована возможность репортить", 500),
]
