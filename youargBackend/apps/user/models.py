from django.db import models
from django.contrib.auth.models import User


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    karma_cached = models.IntegerField(default=0)

    def __str__(self):
        return self.user.__str__()


from ..node.models import Node, NodeImportance


class UserKarma(models.Model):
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE, null=False)
    karma_type = models.CharField(max_length=256, null=False)
    node = models.ForeignKey(Node, on_delete=models.CASCADE, null=True, blank=True)
    importance = models.ForeignKey(NodeImportance, on_delete=models.CASCADE, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, blank=False)

    def __str__(self):
        return "{0} - {1}".format(self.karma_type, self.user.__str__())


from . import model_signals  # handle insert, update, delete logic
