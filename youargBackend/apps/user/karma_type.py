from enum import Enum


class KarmaType:
    def __init__(self, name, short_desc, amount, is_reward):
        self.name = name
        self.short_desc = short_desc
        self.amount = amount
        self.is_reward = is_reward

    @staticmethod
    def get_karma_type(by_name):
        name_string = by_name if type(by_name) == type("") else by_name.value
        for karma_type in KARMA_TYPES:
            if karma_type.name == name_string:
                return karma_type


class KarmaTypeNames(Enum):
    REWARD_CREATE_NODE = "KARMA_REWARD_CREATE_NODE"
    REWARD_VISIT = "KARMA_REWARD_VISIT"
    REWARD_IMPORTANCE_CHECK = "REWARD_IMPORTANCE_CHECK"

    REWARD_REPORT_ACCEPT = "REWARD_REPORT_ACCEPT"
    REWARD_REPORT_DRAW = "REWARD_REPORT_DRAW"
    PENALTY_REPORT_DENY = "REWARD_REPORT_DENY"
    REWARD_REPORT_CHOSEN_RIGHT = "REWARD_REPORT_CHOSEN_RIGHT"
    PENALTY_REPORT_CHOSEN_WRONG = "PENALTY_REPORT_CHOSEN_WRONG"
    PENALTY_REPORT_ACCEPT_FOR_OWNER = "PENALTY_REPORT_ACCEPT_FOR_OWNER"


KARMA_TYPES = [
    KarmaType(KarmaTypeNames.REWARD_CREATE_NODE.value,
              "Создание ноды", 10, True),
    KarmaType(KarmaTypeNames.REWARD_VISIT.value,
              "Посещение сайта каждый день", 5, True),
    KarmaType(KarmaTypeNames.REWARD_IMPORTANCE_CHECK.value,
              "Оценка важности", 2, True),
    KarmaType(KarmaTypeNames.REWARD_REPORT_ACCEPT.value,
              "Репорт подтвержден", 50, True),
    KarmaType(KarmaTypeNames.REWARD_REPORT_DRAW.value,
              "Репорт неопределен", 0, True),
    KarmaType(KarmaTypeNames.PENALTY_REPORT_DENY.value,
              "Репорт опровергнут", 120, False),
    KarmaType(KarmaTypeNames.REWARD_REPORT_CHOSEN_RIGHT.value,
              "Решение репорта выбрано верно", 10, True),
    KarmaType(KarmaTypeNames.PENALTY_REPORT_CHOSEN_WRONG.value,
              "Решение репорта выбрано неверно", 50, False),
]
