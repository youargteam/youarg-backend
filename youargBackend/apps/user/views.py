from django.shortcuts import render, redirect
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.generics import *
from rest_framework.response import Response
from rest_framework.views import APIView
from . import serializers
from django.contrib.auth.models import User
from allauth.socialaccount.models import SocialAccount
from . import karma_type
from . import karma_levels
from . import models


@authentication_classes(TokenAuthentication)
@permission_classes((IsAuthenticated,))
def pass_token(request):
    token = Token.objects.get(user=request.user).key
    return redirect('https://youarg.herokuapp.com/login?token=' + token)

# User


class UserGet(RetrieveAPIView):
    """
    Получить юзера по токену в хедере
    """
    serializer_class = serializers.UserSerializer
    queryset = models.UserProfile.objects

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(queryset, user=self.request.user)
        return obj


# Karma


class KarmaTypeL(ListAPIView):
    """
    Получить список типов поощрения/наказания
    """
    queryset = ""
    serializer_class = serializers.KarmaTypeSerializer
    pagination_class = None

    def list(self, request, *args, **kwargs):
        serializer = self.get_serializer(karma_type.KARMA_TYPES, many=True)
        return Response(serializer.data)


class KarmaTypeSelfR(ListAPIView):
    """
    Получить список поощрений/наказаний для текущего юзера
    """
    serializer_class = serializers.UserKarmaSerializer

    def get_queryset(self):
        return models.UserKarma.objects.filter(user=self.request.user.userprofile)


class KarmaLevelL(ListAPIView):
    """
    Получить список возможностей взависимости от кармы
    """
    queryset = ""
    serializer_class = serializers.KarmaLevelSerializer
    pagination_class = None

    def list(self, request, *args, **kwargs):
        serializer = self.get_serializer(karma_levels.KARMA_LEVELS, many=True)
        return Response(serializer.data)


class KarmaLevelsSelfL(ListAPIView):
    """
    Получить список возможностей текущего юзера
    """
    queryset = ""
    serializer_class = serializers.KarmaLevelSerializer
    pagination_class = None

    def list(self, request, *args, **kwargs):
        user_karma_levels = karma_levels.KarmaLevel.get_karma_levels_self(user=self.request.user.userprofile)
        serializer = self.get_serializer(user_karma_levels, many=True)
        return Response(serializer.data)


