## Описание

Youarg это сервис для более удобной и структурированной организации споров, дебатов.

**Этот проект backend часть Youarg сервиса.**

Базовый url на момент тестирования: 
```
http://youarg.herokuapp.com
```
## Ресурсы

[Техническое задание](https://docs.google.com/document/d/1JEYBHjRu3OTdj_MXG6F3OVaBYYQhN4G8Geq__-nuG5Q/)

## Установка 

Используется Python версии 3.6

Выполнить один раз в `./youargBackend/`:

```
1. pip install virtualenv
2. virtualenv env
3. gem install tmuxinator
4. ./manage.py createsuperuser
5. Настройка social application для allauth
```

## Запуск

```
tmuxinator local
```

## User
### Social providers
Для регистрации и входа используются социальные сети: facebook, vk.

Для регистрации или входа при помощи facebook используется url:
```
/user/facebook/login
```
---
Для регистрации или входа при помощи vk используется url:
```
/user/vk/login
```
---
Обе ссылки при успешном входе/регистрации перенаправят юзера на:
```
[frontend-domain]:[port]/login?token=[token]
```

## Документация

Документация находится по адресу:
```
/api/
```